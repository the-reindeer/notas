#! /bin/bash
# Notas
# Script simples para facil criacao de nptas
# 
# Autor: Rodolfo Almeida	rodolfo.ricardo.almeida@gmail.com
# Data Criacao: 06 de Outubro 2020
# Ultima modificação:

# Definicao do diretorio de arquivo das notas
NOTASDIR=~/docs/notas

# Verificacao e criacao do arquivo
if ! [ -d "$NOTASDIR" ]; 
then
	echo "$NOTASDIR nao existe."
	while true
	do
		read -p "Deseja cria-lo? [S/n] " sn
		case $sn in
			"" | [sS] | [sS][iI][mM])
		echo "Sim"
		echo "A criar diretorio $NOTASDIR ..."
		mkdir -p $NOTASDIR && echo "Directorio criado com sucesso."
		break
		;;
			[nN] | [nN][aA][oO])
		echo "Nao"
		exit 1
		break
		;;
		*)
		 echo "Resposta invalida..."
		;;
		esac
	done
fi

# Estrutura do arquivo de notas
# NOTASDIR
#    |      ano
#    |       |    mes
#    |       |     |   nota-diaria
#
# ano: ano de criacao da nota, <valor em 4 digitos>, xxxx, ex. 2020
# mes: mes de criacao da nota, <valor correspondente ao mes em 2 digitos>-<nome do mes em minusculas>, xx-mes, ex. 10-outubro
# npta-diaria: data de criacao da nota, <ano em 4 digitos><mes em 2 digitos><dia em 2 digitos>, aaaammdd, ex. 20201006

ANO=$(date +%Y)
MES=$(date +%m)
DIA=$(date +%d)
DATA=$(date +%Y%m%d)

case $MES in
	01) MES_EXT="janeiro"
	;;
	02) MES_EXT="fevereiro"
	;;
	03) MES_EXT="marco"
	;;
	04) MES_EXT="abril"
	;;
	05) MES_EXT="maio"
	;;
	06) MES_EXT="junho"
	;;
	07) MES_EXT="julho"
	;;
	08) MES_EXT="agosto"
	;;
	09) MES_EXT="setembro"
	;;
	10) MES_EXT="outubro"
	;;
	11) MES_EXT="novembro"
	;;
	12) MES_EXT="dezembro"
	;;
esac
case $(date +%A) in
	"Monday") DIA_SEMANA="Segunda"
	;;
	"Tuesday") DIA_SEMANA="Terca"
	;;
	"Thursday") DIA_SEMANA="Qaurta"
	;;
	"Wednesday") DIA_SEMANA="Quinta"
	;;
	"Friday") DIA_SEMANA="Sexta"
	;;
	"Saturday") DIA_SEMANA="Sabado"
	;;
	"Sunday") DIA_SEMANA="Domingo"
	;;
esac
NOMENOTA="$ANO$MES$DIA"
PATHNOTA="$NOTASDIR/$ANO/$MES-$MES_EXT"

if ! [ -d $PATHNOTA/ ]
	then 
		echo "A criar diretorios necessarios..."
		mkdir -p $PATHNOTA/
		echo "Directorios criados com sucesso."
fi

if ! [ -f $PATHNOTA/$NOMENOTA ]
	then
		echo "A criar nota..."
		touch $PATHNOTA/$NOMENOTA
		echo "#" >> $PATHNOTA/$NOMENOTA
		echo "# $DIA_SEMANA, $DIA de $MES_EXT $ANO" >> $PATHNOTA/$NOMENOTA
		#echo "# ________________________________" >> $PATHNOTA/$NOMENOTA
		echo "#" >> $PATHNOTA/$NOMENOTA
		echo "Nota criada com sucesso."
fi
echo "" >> $PATHNOTA/$NOMENOTA
echo -n "$(date +%H:%M):" >> $PATHNOTA/$NOMENOTA
echo -ne ' \t '>> $PATHNOTA/$NOMENOTA

vim "+normal G$" +startinsert $PATHNOTA/$NOMENOTA 
#+$(wc $PATHNOTA/$NOMENOTA | awk '{print $1}')
